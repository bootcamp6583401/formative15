package formative.fifteen.formative15;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @PostMapping
    public ResponseEntity<List<Map<String, Object>>> index(@RequestBody String sqlQuery) {
        System.out.println(sqlQuery);
        System.out.println("SELECT * FROM employee");
        System.out.println(sqlQuery.equals("SELECT * FROM employee"));
        try {
            List<Map<String, Object>> bruh = jdbcTemplate.queryForList(sqlQuery);
            return new ResponseEntity<>(bruh, HttpStatus.OK);
        } catch (Exception e) {
            System.out.println("Something's wrong I can feel it.");
            System.out.println(e.getLocalizedMessage());
        }
        return null;
    }

    @PostMapping("/create")
    public void create(@RequestBody String sqlQuery) {
        try {
            jdbcTemplate.execute(sqlQuery);
        } catch (Exception e) {
            System.out.println("Something's wrong I can feel it.");
            System.out.println(e.getLocalizedMessage());
        }
    }

}
