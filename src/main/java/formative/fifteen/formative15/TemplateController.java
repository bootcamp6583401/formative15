package formative.fifteen.formative15;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;

@Controller
public class TemplateController {
    @GetMapping
    public String create() {
        return "create";
    }
}
